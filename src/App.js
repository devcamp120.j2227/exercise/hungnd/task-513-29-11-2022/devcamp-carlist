import { gCarsObj, checkCars } from "./info";

function App() {

  return (
    <div>
      <ul>
        {gCarsObj.map((car, index) => {
          return <li>Tên xe: {car.make + ", biển số: " + car.vID}, tình trạng: {checkCars(car)}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
